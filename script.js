let perguntas;

httpRequest = new XMLHttpRequest();
httpRequest.open('GET', 'https://quiz-trainee.herokuapp.com/questions', true);
httpRequest.send();

httpRequest.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
        perguntas = JSON.parse(this.responseText);
        console.log(perguntas);
    }
}

var perguntaAtual = -1
var pontos = 0
var spansRespostas = document.getElementsByTagName("span")
var botoesRespostas = document.getElementsByName("resposta")


function mostrarQuestao() {
    //mostra as perguntas e suas respectivas opções de respostas
    if (perguntaAtual == -1) {
        document.getElementById("resultado").style.display = "none"
        document.getElementById("respostas").style.display = "inline"
        document.getElementById("confirmar").innerHTML = "PRÓXIMA"
    } 
    else{ //se o quiz já tiver sido iniciado
        for (var i = 0; i < 4; i++)
            //irá conferir se uma opção foi marcada
            if (document.getElementsByName("resposta")[i].checked){ 
                //irá lhe atribuir uma pontuação
                if (botoesRespostas[i].checked) {
                    pontos += Number(botoesRespostas[i].value)
                    botoesRespostas[i].checked = false 
                }
                break //irá para a próxima pergunta
            }
            else if(i == 3) //se nenhuma opção for marcada continuará nessa mesma pergunta
                return
    }

    perguntaAtual++

    //se não houver mais perguntas o quiz será finalizado
    if (perguntaAtual >= perguntas.length) {
        finalizarQuiz();
        return;
    }

    //atualiza o título da pergunta
    document.getElementById('título').innerHTML = perguntas[perguntaAtual].title

    for (var i = 0; i < perguntas[perguntaAtual].options.length; i++) {
        //atualiza tanto as respostas como os valores das respostas
        spansRespostas[i].innerHTML = perguntas[perguntaAtual].options[i].answer
        spansRespostas[i].parentElement.children[0].value = perguntas[perguntaAtual].options[i].value
    }
}

function finalizarQuiz() {
    //calcula a porcentagem e exibe o resultado final
    var porcentagemFinal = Math.round((pontos / 15) * 100)
    perguntaAtual = -1
    pontos = 0
    document.getElementById('título').innerHTML = 'QUIZ DOS VALORES DA GTI'
    document.getElementById("respostas").style.display = "none"
    document.getElementById("resultado").style.display = 'block'
    document.getElementById("resultado").innerHTML = 'SUA PONTUAÇÃO: ' + porcentagemFinal + '%'
    document.getElementById("confirmar").innerHTML = "REFAZER QUIZ";
}
